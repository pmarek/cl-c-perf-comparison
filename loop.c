#include <stdio.h>
#include <stdlib.h>


int main (int argc, char *args[])
{
	long max, i, sum, nr, j;

	nr = atol(args[2]);
	max = atol(args[1]);

	for(j=0; j<nr; j++) {
		sum = j;
		for(i=1; i<max; i++)
			sum+= i;

		printf("%d: %lx\n", j, sum);
	}
	return (sum == 15);
}
