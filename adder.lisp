

(defun main ()
  (let ((max (parse-integer (second sb-ext:*posix-argv*)))
        (nr (parse-integer (third sb-ext:*posix-argv*))))
    (declare (type (integer 0 #. (floor most-positive-fixnum 2)) max)
             (optimize (safety 0)
                       (speed 3) 
                       (debug 0)))
    ;; Ensure buffering
    ;(print
    ;  (with-output-to-string (*standard-output*)
        (dotimes (j nr)
          (let ((sum j))
            (declare (type (integer 0 #. (floor most-positive-fixnum 2)) sum))
            (loop for i fixnum below max
                  do (incf sum i))
            (format t "~d: ~x~%" j sum)))))
    ;))
