
CFLAGS=-O2 -g
MAX=2100000000

all: loop cl
	# /usr/bin/time --format "%U" ./loop $(MAX)
	# /usr/bin/time --format "%U" ./cl $(MAX)
	time ./loop $(MAX) 4
	time ./cl $(MAX) 4

loop: loop.c

cl: adder.lisp
	sbcl --no-userinit --load "$<" --eval "(sb-ext:save-lisp-and-die \"./$@\" :compression nil :executable t :toplevel #'main)"
